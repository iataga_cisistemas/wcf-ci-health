﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.ServiceModel.Web;
using System.Text;


namespace WcfCiHealth
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ISEmpresa" in both code and config file together.
    [ServiceContract]
    public interface ISEmpresa
    {
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/ListaEmpresas", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<Empresa> GetPesquisarEmpresa();

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/Empresas", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        String GetCadastraEmpresa(String dsorg);
    }
}
