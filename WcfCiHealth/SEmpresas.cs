﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WcfCiHealth
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "SEmpresa" in both code and config file together.
    public class SEmpresas : ISEmpresa
    {
        CIHealthEntities context = new CIHealthEntities();
        public List<Empresa> GetPesquisarEmpresa()
        {
            return context.Empresas.ToList();
        }
        public String GetCadastraEmpresa(String dsorg)
        {

            try { 
                var empresa = new Empresa();
                empresa.dsorg = dsorg;
                context.Empresas.Add(empresa);
                context.SaveChanges();
                return "Cadastrado Com Sucesso";
            }catch(Exception e){
                return "Não Foi Possivel Realizar o Casdastro";
            }


        }
    }

}
